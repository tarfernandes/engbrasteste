const { request, response } = require('express');
const express = require ('express');

const app = express();

app.use(express.json());

app.get('/', (request, response) => {
  return response.json('Hello World');
});

app.post('/teste', (request, response) => {
  return response.json(request.body);
});

app.listen(3000, () => {
  console.log('Backend Iniciado!')
});

/*Criar um micro serviço REST com NodeJS e express, com dois
endpoints:
[GET] = Hello World [FEITO] 
[POST] = Que retorne o conteúdo do “body” [FEITO]
Testar os endpoints utilizando Insominia, Postman (ou alguma outra
ferramenta de teste de requisições) [FEITO]
*/ 